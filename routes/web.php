<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Latihancontroller;
use App\Http\Controllers\PostController;
use App\Http\Controllers\SiswaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get(' about ', function () {
    return view('tentang');
});

route::get(' /profile ', function () {
    $nama = "dida";
    return view(' pages.profile ', compact( 'nama' ));
});

route::get(' /biodata ', function () {
    $nama = "dida";
    $agama = "islam";
    $hobi = "main game,hiking";
    $moto = "turu";
    $cita = "jadi presiden";
    return view(' pages.biodata ', compact( 'nama','agama', 'hobi','moto','cita'));
});

route::get(' bio/{nama} ', function ($d) {
    return view(' pages.bio ', compact( 'd' ));
});

route::get(' order/{makanan}/{minuman}/{harga}', function ($d,$c,$b) {
    return view(' pages.order ', compact( 'd','c','b' ));
});

route::get(' pesan/{pesanan?}', function ($a = "-") {
    return view(' pages.pesan ', compact( 'a' ));
});

route::get(' open/{makanan?}/{minuman?}/{cemilan?}', function ($makanan = "-",$minuman = "-",$cemilan = "-") {
    return view(' pages.open ', compact( 'makanan','minuman','cemilan' ));
});

Route::get('pesanan/{a?}/{b?}/{c?}', function ($a = "silakan masukkan pesanan" ,$b = null, $c = null) {
    return view('pages.pesanan' ,compact('a','b','c'));

});

route::get('latihan',[Latihancontroller::class,'perkenalan']);

route::get('halo',[Latihancontroller::class,'kenalan']);

route::get('nama',[Latihancontroller::class,'siswa']);

route::get('dosen',[Latihancontroller::class,'dosen']);

route::get('acara',[Latihancontroller::class,'acara']);

route::get('toko',[Latihancontroller::class,'shop']);

route::get('/penilaian',[Latihancontroller::class,'kelas']);

route::get('/post',[PostController::class,'tampil']);

route::get('post/{id}',[PostController::class,'search']);

route::get('post/judul/{title}',[PostController::class,'search_title']);

route::get('post/edit/{id}/{title}/{content}',[PostController::class,'edit']);

route::get('post/create/{title}/{content}',[PostController::class,'create']);

route::get('post/delete/{id}',[PostController::class,'hapus']);

route::get('/sekolah/siswa',[SiswaController::class,'tampil']);

route::get('/post/{id}',[SiswaController::class,'search']);

route::get('post/create/{id}',[SiswaController::class,'create']);