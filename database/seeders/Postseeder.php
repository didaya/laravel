<?php


namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class Postseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            ['title' => 'belajar huruf ijaiyah',
                'content' => 'loren ipsum sit amet dolor'],
            ['title' => 'indonesia kapan masuk piala dunia?',
                'content' => 'loren ipsum sit amet dolor',],
            ['title' => 'tahu banung',
                'content' => 'ngeunah',]
        ];
        DB::table('posts')->insert ($sample);
    }
}
