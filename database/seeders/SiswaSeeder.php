<?php

namespace Database\Seeders;
use DB;

use Illuminate\Database\Seeder;

class SiswaSeeder extends Seeder    
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sample = [
            ['nama' => 'pesa nidrun',
                'kelas' => 'XI',
                'umur' => '17',
                'jenis_kelamin' => 'laki - laki',
                'jurusan' => 'RPL'],

            ['nama' => 'jajang garpit',
                'kelas' => 'XI',
                'umur' => '18',
                'jenis_kelamin' => 'laki - laki',
                'jurusan' => 'TBSM'],

            ['nama' => 'asep banung',
                'kelas' => 'XI',
                'umur' => '19',
                'jenis_kelamin' => 'laki - laki',
                'jurusan' => 'TKRO'],

                ['nama' => 'asep garut',
                'kelas' => 'XI',
                'umur' => '19',
                'jenis_kelamin' => 'laki - laki',
                'jurusan' => 'TBSM'],

                ['nama' => 'asep sawah',
                'kelas' => 'XI',
                'umur' => '19',
                'jenis_kelamin' => 'laki - laki',
                'jurusan' => 'RPL']

        ];
        DB::table('siswas')->insert ($sample);
    }
}
