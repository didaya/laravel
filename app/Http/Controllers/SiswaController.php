<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
class SiswaController extends Controller
{
    public function tampil()
    {
        $sekolah = Siswa::all();
        return view('post.siswa', compact('sekolah'));
    }
}
