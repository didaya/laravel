<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Latihancontroller extends Controller
{
    public function perkenalan()
    {
        $nama = "DIDA ADITYA"; 
        $alamat = "sekeawi";
        $umur = 16;

        return view('pages.perkenalan',compact('nama','alamat','umur'));
    }
        public function siswa()
        {
            $a = [
            
                array('id' => 1,'name' => 'dadang','age' => 15, 'hobi' => ['puksal','turu','bernyanyi']),
                array('id' => 2,'name' => 'dadang','age' => 18,'hobi' =>[ 'golf','mancing mania']),
               
    
            ];
            return view('pages.siswa',['siswa' => $a]);
        }
        public function dosen()
        {
            $a = [
            
                array('id' => 1,'name' => 'yusuf bachtiar','matkul' => 'pemrograman mobile', 'mahasiswa' => 
                [
                  ['namad' => 'muhammad soleh','nilai' => 78],
                  ['namad' => 'jujun junaedi','nilai' => 85 ],
                  ['namad' => 'mamat alkatiri', 'nilai' => 90 ]

            ]),

                array('id' => 2,'name' => 'yaris riyadi','matkul' => 'pemrograman web','mahasiswa' =>
                [
                    ['namad' => 'alfred Mactominay','nilai' => 67],
                    ['namad' => 'yusuf kala','nilai' => 90]


            ]),
            ];
            return view('pages.dosen',['dosen' => $a]);
        }
        public function acara()
        {
            $a = [
            
                array('id' => 1,'nama_acara' => 'GTV',"Jadwal" => 
                [
                  ['nama' => 'NARUTO','jam' => '17:00']
                ]),
                array('id' => 1,'nama_acara' => 'TRANS 7', "Jadwal" => 
                [
                  ['nama' => 'Bioskop trans','jam' => "20:00"]
                ]),
                array('id' => 1,'nama_acara' => 'TVRI', "Jadwal" => 
                [
                  ['nama' => 'wayang','jam' => '23:00']
                ]),
                array('id' => 1,'nama_acara' => 'NET.TV', "Jadwal" => 
                [
                  ['nama' => 'tonight show','jam' => '00:00']
                ]),
                array('id' => 1,'nama_acara' => 'indosiar', "Jadwal" => 
                [
                  ['nama' => 'azab','jam' => "11:00"]
                ])
                
            ];
            return view('pages.acara',['acara' => $a]);
        }
        public function shop()
        {
            $belanja = [
            
                array('name' => 'alfian',
                'belanjaan' => 
                [
                  ['jenis' => ' sepatu','merk' => 'VANS','harga' => 250000],
                  ['jenis' => ' baju','merk' => 'ERIGO','harga' => 100000],
                  ['jenis' => ' celana','merk' => 'ERIGO','harga' => 150000],
                  ['jenis' => ' kupluk','merk' => 'VANS','harga' => 100000]

            ]
        ),

                array('name' => 'USEP',
                'belanjaan' => 
                [
                    ['jenis' => ' sepatu','merk' => 'cons','harga' => 100000],
                    ['jenis' => ' baju','merk' => 'ERIGO','harga' => 75000],
                    ['jenis' => ' celana','merk' => 'ERIGO','harga' => 125000]


            ])
            ];
            return view('pages.shop',['belanja' => $belanja]);
        }
        public function kelas()
        {
            $nilai = [
            
                array('name' => 'agus','jurusan' => 'TKR',
                'data' => 
                [
                  ['mapel' => 'bahasa indonesia','nilai' => 80],
                  ['mapel' => 'bahasa inggris','nilai' => 97],
                  ['mapel' => 'produktif kejuruan','nilai' => 67],
                  ['mapel' => 'matematika','nilai' => 100],

            ]
        ),

                array('name' => 'mahmud','jurusan' => 'TKR',
                'data' => 
                [
                ['mapel' => 'bahasa indonesia','nilai' => 78],
                ['mapel' => 'bahasa inggris','nilai' => 86],
                ['mapel' => 'produktif kejuruan','nilai' => 90],
                ['mapel' => 'matematika','nilai' => 67],

            ]
        ),

                array('name' => 'rendi','jurusan' => 'TSM',
                'data' => 
                [
                ['mapel' => 'bahasa indonesia','nilai' => 90],
                ['mapel' => 'bahasa inggris','nilai' => 50],
                ['mapel' => 'produktif kejuruan','nilai' => 65],
                ['mapel' => 'matematika','nilai' => 78],

            ]
        ),

                array('name' => 'rendi','jurusan' => 'RPL',
                'data' => 
                [
                ['mapel' => 'bahasa indonesia','nilai' => 78],
                ['mapel' => 'bahasa inggris','nilai' => 90],
                ['mapel' => 'produktif kejuruan','nilai' => 56],
                ['mapel' => 'matematika','nilai' => 78],

            ]
        ),

        array('name' => 'abdul','jurusan' => 'RPL',
        'data' => 
        [
        ['mapel' => 'bahasa indonesia','nilai' => 89],
        ['mapel' => 'bahasa inggris','nilai' => 67],
        ['mapel' => 'produktif kejuruan','nilai' => 80],
        ['mapel' => 'matematika','nilai' => 90],

        ]
        )
            ];
            return view('pages.penilaian',['nilai' => $nilai]);
        }
        
    }


  