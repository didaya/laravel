<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <fieldset>
        <legend>Data post</legend>
        <br>
        <table border ="3">
            <tr>
                <td>nomor</td>
                <td>id</td>
                <td>nama</td>
                <td>kelas</td>
                <td>umur</td>
                <td>jenis kelamin</td>
                <td>jurusan</td>
            </tr>
            @php $no=1; @endphp
            @foreach ($sekolah as $data)
                <tr>
                    <td>{{$no++}}</td>
                    <td>{{$data->id}}</td>
                    <td>{{$data->nama}}</td>
                    <td>{{$data->kelas}}</td>
                    <td>{{$data->umur}}</td>
                    <td>{{$data->jenis_kelamin}}</td>
                    <td>{{$data->jurusan}}</td>
                </tr>
                @endforeach
        </table>
    </fieldset>
</body>
</html>